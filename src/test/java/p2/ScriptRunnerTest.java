package p2;

import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ScriptRunnerTest {
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	
	@Test
	public void shouldReturnSaneOrderingOfScriptExecution() {
		List<Integer> s2Dep = Arrays.asList(1);
		List<Integer> s3Dep = Arrays.asList(1, 4, 6);
		List<Integer> s4Dep = Arrays.asList(6);
		List<Integer> s5Dep = Arrays.asList(2, 6);
		List<Integer> s6Dep = Arrays.asList(2);
		
		Script s1 = new Script(1, new ArrayList<>());
		Script s2 = new Script(2, s2Dep);
		Script s3 = new Script(3, s3Dep);
		Script s4 = new Script(4, s4Dep);
		Script s5 = new Script(5, s5Dep);
		Script s6 = new Script(6, s6Dep);
		int[] expectedSaneOrder = {1, 2, 6, 5, 4, 3};
		Script[] allScripts = {s1, s2, s3, s4, s5, s6};
		ScriptRunner scriptRunner = new ScriptRunner();
		int[] saneOrder = scriptRunner.runOrder(allScripts);
		assertArrayEquals(expectedSaneOrder, saneOrder);
	}
	
	@Test
	public void throwsRuntimeExceptionIfThereIsCycleInScripts() {
		List<Integer> s2Dep = Arrays.asList(1);
		List<Integer> s3Dep = Arrays.asList(1, 4, 6);
		List<Integer> s4Dep = Arrays.asList(6);
		List<Integer> s5Dep = Arrays.asList(2, 6);
		List<Integer> s6Dep = Arrays.asList(2);
		List<Integer> s7Dep = Arrays.asList(7);
		
		Script s1 = new Script(1, new ArrayList<>());
		Script s2 = new Script(2, s2Dep);
		Script s3 = new Script(3, s3Dep);
		Script s4 = new Script(4, s4Dep);
		Script s5 = new Script(5, s5Dep);
		Script s6 = new Script(6, s6Dep);
		Script s7 = new Script(7, s7Dep);
		Script[] allScripts = {s1, s2, s3, s4, s5, s6, s7};
		expectedException.expect(RuntimeException.class);
		expectedException.expectMessage("Scripts cyclically depend on each other. No sane order to execute scripts exists.");
		ScriptRunner sRunner = new ScriptRunner();
		sRunner.runOrder(allScripts);
		
	}
	
	@Test //simple trivial test to show no solution exist if there are 2 scripts depending on each other
	public void throwsRuntimeExceptionForCycleBtwn2Scripts() {
		List<Integer> s1Dep = Arrays.asList(2);
		List<Integer> s2Dep = Arrays.asList(1);
		Script s1 = new Script(1, s1Dep);
		Script s2 = new Script(2, s2Dep);
		Script[] allScripts = {s1, s2};
		expectedException.expect(RuntimeException.class);
		expectedException.expectMessage("At least one script must have no dependencies in order to find any sane ordering of script execution");
		ScriptRunner sRunner = new ScriptRunner();
		sRunner.runOrder(allScripts);
		
	}
	
	
	@Test
	public void shouldReturnOneScriptOnly() {
		Script s1 = new Script(1, new ArrayList<>());
		Script[] allScripts = {s1};
		ScriptRunner scriptRunner = new ScriptRunner();
		int[] saneOrder = scriptRunner.runOrder(allScripts);
		int[] expectedSaneOrder = {1};
		assertArrayEquals(expectedSaneOrder, saneOrder);
	}
	
	@Test //s1--->s2
	public void shouldReturn2Scripts() {
		List<Integer> s2Dep = Arrays.asList(1);
		
		Script s1 = new Script(1, new ArrayList<>());
		Script s2 = new Script(2, s2Dep);
		ScriptRunner scriptRunner = new ScriptRunner();
		Script[] allScripts = {s1, s2};
		
		int[] saneOrder = scriptRunner.runOrder(allScripts);
		int[] expectedSaneOrder = {1, 2};
		assertArrayEquals(expectedSaneOrder, saneOrder);
	}
	
	@Test
	public void shouldReturnEmptyArray() {
		ScriptRunner scriptRunner = new ScriptRunner();
		Script[] allScripts = {};
		int[] saneOrder = scriptRunner.runOrder(allScripts);
		int[] expectedSaneOrder = {};
		assertArrayEquals(expectedSaneOrder, saneOrder);
	}
	
	@Test
	public void throwsRuntimeExceptionIfNoScriptHasZeroDependencies() {
		List<Integer> s1Dep = Arrays.asList(2);
		List<Integer> s2Dep = Arrays.asList(1);
		List<Integer> s3Dep = Arrays.asList(1, 4, 6);
		List<Integer> s4Dep = Arrays.asList(6);
		List<Integer> s5Dep = Arrays.asList(2, 6);
		List<Integer> s6Dep = Arrays.asList(2);
		
		Script s1 = new Script(1, s1Dep);
		Script s2 = new Script(2, s2Dep);
		Script s3 = new Script(3, s3Dep);
		Script s4 = new Script(4, s4Dep);
		Script s5 = new Script(5, s5Dep);
		Script s6 = new Script(6, s6Dep);
	
		Script[] allScripts = {s1, s2, s3, s4, s5, s6};
		expectedException.expect(RuntimeException.class);
		expectedException.expectMessage("At least one script must have no dependencies in order to find any sane ordering of script execution");
		ScriptRunner sRunner = new ScriptRunner();
		sRunner.runOrder(allScripts);
	}
}