package p1;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class AnagramsTest {
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void throwsIllegalArgumentExceptionForNullWords() {
		String left=null;
		String right = null;
		Anagrams anagrams = new Anagrams();
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Either 1 or both of the words are empty or null");
		anagrams.check(left, right);
	}
	
	@Test
	public void throwsIllegalArgumentExceptionForEmptyWords() {
		String left="";
		String right ="";
		Anagrams anagrams = new Anagrams();
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Either 1 or both of the words are empty or null");
		anagrams.check(left, right);
	}
	
	@Test
	public void shouldDetectInvalidAnagramsFromUnequalSizeWords() {
		String left="Arc";
		String right = "ArcZ";
		Anagrams anagrams = new Anagrams();
		assertFalse(anagrams.check(left, right));
	}
	
	@Test
	public void shouldSuccessfullyDetectAnagrams() {
		String left = "Elbow";
		String right	 = "Below";
		Anagrams anagrams = new Anagrams();
		assertTrue(anagrams.check(left, right));
	}
	
	@Test
	public void shouldSuccessfullyDetectTwoWordsAreNotAnagrams() {
		String left = "Mondayu";
		String right = "Tuesday";
		Anagrams anagrams = new Anagrams();
		assertFalse("Unequal string sizes!", anagrams.check(left, right));
	}
	
	@Test
	public void shouldReturnFalseForSameWord() {
		String left = "LONELY";
		String right = "lonely";
		Anagrams anagrams = new Anagrams();
		assertFalse("Same word is not anagram by anagram definition", anagrams.check(left, right));
	}
	
	@Test
	public void shouldSuccessfullyDetectAnagramPhrase() {
		String left = "Dormitory";
		String right = "dirty Room";
		Anagrams anagrams = new Anagrams();
		assertTrue(anagrams.check(left, right));
	}
	
	@Test
	public void shouldSuccessfullyDetectAnagramPhrase2() {
		String left ="Eleven plus two";
		String right = "Twelve plus one";
		Anagrams anagrams = new Anagrams();
		assertTrue(anagrams.check(left, right));
	}
}