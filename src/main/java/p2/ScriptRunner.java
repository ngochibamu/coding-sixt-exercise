package p2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ScriptRunner {

    public int[] runOrder(Script[] allScripts) {
    	
    	LinkedList<Script> queue = new LinkedList<>();
    	Map<Script, Integer> indegreeMap = new HashMap<>();
    	
    	//if list of scripts is empty, no need to proceed
    	if(allScripts.length == 0)
    		return new int[0];
    	   	
    	for(int i = 0; i < allScripts.length; i++) {
    		int numDependencies = allScripts[i].getDependencies().size();
    		indegreeMap.put(allScripts[i], numDependencies);
    		if(numDependencies == 0)
    			queue.add(allScripts[i]);
    	}
    	//for the scripts to have a sane order of execution, there must exist at least 1 script with zero dependencies, otherwise if such script
    	//does not exist then there is no need to continue, just throw runtime exception
    	if(queue.isEmpty())
    		throw new RuntimeException("At least one script must have no dependencies in order to find any sane ordering of script execution");
    	
    	List<Script> scriptOrder = new ArrayList<>();
    	while(!queue.isEmpty()) {
    		Script current = queue.pollLast();
    		scriptOrder.add(current);
    		
    		List<Script> adjacentScripts = getAdjacentScripts(current, allScripts);
    		for(Script adjScript: adjacentScripts) {
    			int updatedIndegree  = indegreeMap.get(adjScript) - 1;
    			indegreeMap.put(adjScript, updatedIndegree);
    			
    			if(updatedIndegree == 0)
    				queue.add(adjScript);
    		}
    	}
    	if(scriptOrder.size() != allScripts.length)
    		throw new RuntimeException("Scripts cyclically depend on each other. No sane order to execute scripts exists.");
    	return scriptOrder.stream().mapToInt(Script::getScriptId).toArray();
    }
    
    public List<Script> getAdjacentScripts(Script currentScript, Script[] allScripts) {
    	
    	return Stream.of(allScripts)
    		.filter(sc -> sc.getScriptId() != currentScript.getScriptId())
    		.filter(sc -> sc.getDependencies().contains(currentScript.getScriptId())).collect(Collectors.toList());
    }
}
