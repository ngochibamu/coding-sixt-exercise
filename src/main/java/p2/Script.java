package p2;

import java.util.List;

public class Script {

    private final int scriptId;
    private final List<Integer> dependencies;

    public Script(int scriptId, List<Integer> dependencies) {
        this.scriptId = scriptId;
        this.dependencies = dependencies;
    }

    public int getScriptId() {
        return scriptId;
    }

    public List<Integer> getDependencies() {
        return dependencies;
    }

}