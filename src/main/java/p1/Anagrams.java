package p1;

import java.util.Map;
import java.util.stream.Collectors;

public class Anagrams {

    public boolean check(String left, String right) {
    	
    	if(left == null || right == null || left=="" || right == "") {
    		throw new IllegalArgumentException("Either 1 or both of the words are empty or null");
    	}

    	left = left.replaceAll("\\s", "");
    	right = right.replaceAll("\\s", "");
    	if(left.length() != right.length() || (left.equalsIgnoreCase(right))) {
    		return false;
    	}
    	    	
    	final char[] leftSeq = left.toLowerCase().toCharArray();
    	final Map<Character, Integer> leftSeqFreq = left.toLowerCase().chars().boxed().collect(Collectors.toMap(
    			k -> Character.valueOf((char) k.intValue()),
    			v -> 1,
    			Integer::sum
    	));
    	final char[] rightSeq = right.toLowerCase().toCharArray();
    	for(int i = 0; i < leftSeq.length; i++) {
    		if(leftSeqFreq.containsKey(rightSeq[i])){
    			int freq = leftSeqFreq.get(rightSeq[i]);
    			if(freq == 0) {
    				return false;
    			}else {
    				freq--;
    				leftSeqFreq.put(rightSeq[i], freq);
    			}
    		}else {
    			return false;
    		}
    	}
		return true;
    }
 }
