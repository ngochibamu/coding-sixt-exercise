# Sixt Hiring Test


### Problem 1 - Anagrams
Implement the logic to detect a pair of anagrams.
An anagram is direct word switch or word play,
the result of rearranging the letters of a word or phrase to produce a new word or phrase,
using all the original letters exactly once; for example, the word �anagram' can be rearranged into �nagaram�

##### Solution
The time complexity of this solution is O(n) which is better than a more straight forward solution based on sorting the words first. Arrays.sort(word) is of order 
n*log n, which is why I preferred this implementation that uses extra space.


### Problem 2 - Script Runner
Let's say we have a database of scripts:
each script has an arbitrary number of dependencies.
The dependencies are expressed as a list of scriptIds that need to be executed before a given script.
We want to come up with an execution plan so that we can run all of the scripts in a sane order.
The script is represented by: /src/main/java/p2/ScriptRunner.java

##### Solution
In this problem I assume that there is no cyclic dependency between the scripts. If there were to be a cycle, then I throw a runtime exception with a more useful message. Why did I do this because, if you have script1 depending on script2 and script2 depending on script1, then you have deadlock situation, there is no practical solution to finding an order of execution that makes sense. 

Time complexity is O(V+E) where V is the number scripts and E is the number of connections between the scripts.

Once you are done with the task, please inform us over email
